public class Ejercicio5App {
	public static void main(String[] args) {
		int a = 10;
		int b = 20;
		int c = 30;
		int d = 40;
		
		System.out.println("Valor inicial de A = " + a);
		System.out.println("Valor inicial de B = " + b);
		System.out.println("Valor inicial de C = " + c);
		System.out.println("Valor inicial de D = " + d);
		
		//B toma el valor de C
		b = c;
		
		//C toma el valor de A
		c = a;
		
		//A toma el valor de D
		a = d;
		
		//D toma el valor de B
		d = b;
		
		System.out.println("");
		System.out.println("Valor final de A = " + a);
		System.out.println("Valor final de B = " + b);
		System.out.println("Valor final de C = " + c);
		System.out.println("Valor final de D = " + d);
	}
}